package com.example.latihan.Latihan10.Controller;

import com.example.latihan.Latihan10.Service.KaryawanTemplateService;
import com.example.latihan.Latihan6.entitiy.Karyawan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("binar/rt")
public class KaryawanTemplateController {

    @Autowired
    private KaryawanTemplateService servis;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Karyawan karyawan) {
        Map obj = servis.insert(karyawan);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody Karyawan karyawan){
        karyawan.setId(id);
        Map obj = servis.update(karyawan);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    @ResponseBody
    public ResponseEntity<Map> getAll(){
        Map k = servis.getAll();
        return new ResponseEntity<Map>(k, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable("id") Long id) {
        Map obj = servis.delete(id);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }
}
