package com.example.latihan.Latihan10.Service.Impl;

import com.example.latihan.Latihan10.Service.KaryawanTemplateService;
import com.example.latihan.Latihan6.entitiy.Karyawan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class KaryawanTemplateImpl implements KaryawanTemplateService {

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public Map insert(Karyawan karyawan){
        Map map = new HashMap();
        try{
            String url = "http://localhost:9090/binar/karyawan/save";
            ResponseEntity<Map> result = restTemplateBuilder.build().postForEntity(url, karyawan, Map.class);


            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses melakukan save");
            return map;// respon
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
    @Override
    public Map update(Karyawan karyawan){
        Map map = new HashMap();
        try{
            String url = "http://localhost:9090/binar/karyawan/update/"+karyawan.getId();

            HttpEntity<Karyawan> req = new HttpEntity<>(karyawan);
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.PUT, req, new ParameterizedTypeReference<Map>(){});
            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Karyawan berhasil diupdate");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
    @Override
    public Map getAll() {
        List<Karyawan> karyawans = new ArrayList<Karyawan>();
        Map map = new HashMap();
        try {
            String url = "http://localhost:9090/binar/karyawan/all";

            ResponseEntity<Map> result = restTemplateBuilder.build()
                    .exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Map>(){});
            map.put("data", result.getBody());
            map.put("statusCode", 200);
            map.put("statusMessage", "Sukses getAll");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;// eror
        }
    }
    @Override
    public Map delete(Long idKaryawan){
        Map map = new HashMap();
        try {
            String url = "http://localhost:9090/binar/karyawan/delete/"+ idKaryawan;

            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.DELETE, null, new ParameterizedTypeReference<Map>() {});
            map.put("statusCode", "200");
            map.put("statusMessage", "Karyawan berhasil dihapus");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}
