package com.example.latihan.Latihan10.Service;

import com.example.latihan.Latihan6.entitiy.Karyawan;

import java.util.Map;

public interface KaryawanTemplateService {


    public Map insert(Karyawan karyawan);

    public Map update(Karyawan karyawan);

    public Map getAll();

    public Map delete(Long idKaryawan);

}
