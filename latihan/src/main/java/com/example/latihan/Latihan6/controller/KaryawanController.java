package com.example.latihan.Latihan6.controller;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.example.latihan.Latihan6.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/binar/karyawan")
public class KaryawanController {

    @Autowired
    KaryawanService servis;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map karyawans = servis.getAll();
        return new ResponseEntity<Map>(karyawans, HttpStatus.OK);
    }

    @GetMapping("/all/{status}")
    @ResponseBody
    public ResponseEntity<Map> getBystatus(@PathVariable(value = "status")int status) {
        Map karyawans = servis.getByStatus(status);
        return new ResponseEntity<Map>(karyawans, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Karyawan newKaryawan) {
        Map map = new HashMap();
        Map kryw = servis.insert(newKaryawan);

        map.put("Request= ", newKaryawan);
        map.put("Response ", kryw);
        return new ResponseEntity<Map>(kryw, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody Karyawan karyawanUpdate) {
        karyawanUpdate.setId(id);
        Map kryw = servis.update(karyawanUpdate);

        Map map = new HashMap();

        map.put("Request =", karyawanUpdate);
        map.put("Response =", kryw);
        return new ResponseEntity<Map>(kryw, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map kryw = servis.delete(id);
        return new ResponseEntity<Map>(kryw, HttpStatus.OK);
    }
}
