package com.example.latihan.Latihan6.controller.Thymeleaf;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.example.latihan.Latihan6.repository.KaryawanRepo;
import com.example.latihan.Latihan6.service.KaryawanService;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/binar/view/karyawan")
public class ThymeleafController {

    @Autowired
    public KaryawanService service;

    @Autowired
    public KaryawanRepo repo;


    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("title", "Selamat Datang");
        return "index";
    }



}
