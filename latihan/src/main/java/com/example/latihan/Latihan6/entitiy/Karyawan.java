package com.example.latihan.Latihan6.entitiy;

import com.example.latihan.Latihan7.Entity.AbstractDate;
import com.example.latihan.Latihan7.Entity.KaryawanDetail;
import com.example.latihan.Latihan7.Entity.Rekening;
import com.example.latihan.Latihan7.Entity.TrainingKaryawan;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "karyawan")
@Where(clause = "deleted_date is null")
public class Karyawan extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nama", nullable = false, length = 45)
    private String nama;

    @Column(name = "jenisKelamin", nullable = false, length = 1)
    private char jenisKelamin;

    @Column(name = "tanggalLahir", nullable = false, length = 10)
    private LocalDate tanggalLahir;

    @Column(name = "alamat", nullable = false, length = 45)
    private String alamat;

    @Column(name = "status", nullable = false, length = 1)
    private int status;

    @OneToOne(mappedBy = "karyawan")
    private KaryawanDetail karyawanDetail;

    @OneToMany(mappedBy = "karyawan", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rekening> rekening;

    @OneToMany(mappedBy = "karyawan")
    private List<TrainingKaryawan> trainingKaryawan;

    public Karyawan(){

    }

    public Karyawan(String nama, char jenisKelamin, LocalDate tanggalLahir, String alamat, int status, KaryawanDetail karyawanDetail){
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.tanggalLahir = tanggalLahir;
        this.alamat = alamat;
        this.status = status;
        this.karyawanDetail = karyawanDetail;
    }
    public KaryawanDetail getKaryawanDetail(){
        return karyawanDetail;
    }
    public List<Rekening> getRekening(){
        return rekening;
    }
}
