package com.example.latihan.Latihan6.repository;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KaryawanRepo extends JpaRepository<Karyawan, Long> {

    @Query("select k from Karyawan k")
    public List<Karyawan> getAllKaryawan();

    @Query("select k from Karyawan k WHERE k.id = :id")
    public Karyawan getById(@Param("id") Long id);

    @Query("select k from Karyawan k WHERE k.status = :status")
    public List<Karyawan> getByStatus(@Param("status") int status);


}
