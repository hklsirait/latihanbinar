package com.example.latihan.Latihan6.service;

import com.example.latihan.Latihan6.entitiy.Karyawan;

import java.util.Map;

public interface KaryawanService {

    public Map getAll();

    public Map getByStatus(int status);

    public Map insert(Karyawan karyawan);

    public Map update(Karyawan karyawan);

    public Map delete(Long karyawanId);

    public long count();
}
