package com.example.latihan.Latihan6.service.impl;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.example.latihan.Latihan6.repository.KaryawanRepo;
import com.example.latihan.Latihan6.service.KaryawanService;
import com.example.latihan.Latihan7.Entity.KaryawanDetail;
import com.example.latihan.Latihan7.Repository.KaryawanDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

@Service
@Transactional
public class KaryawanImpl implements KaryawanService {

    @Autowired
    public KaryawanRepo repo;

    @Autowired
    public KaryawanDetailRepository repoKaryawanDetail;

    @Override
    public Map getAll() {
        List<Karyawan> karyawans = new ArrayList<Karyawan>();
        Map map = new HashMap();
        try{
            karyawans = repo.getAllKaryawan();
            map.put("data", karyawans);
            map.put("statusCode", "200");
            map.put("statusMessage", "Succes");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getByStatus(int status) {
        List<Karyawan> karyawans = new ArrayList<Karyawan>();
        Map map = new HashMap();
        try {
            karyawans = repo.getByStatus(status);
            map.put("data", karyawans);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", "Success");
            return map;
        }
    }
    @Override
    public Map insert(Karyawan karyawan) {
        Map map = new HashMap();
        try {

            KaryawanDetail karyawanDetail = repoKaryawanDetail.save(karyawan.getKaryawanDetail());
            Karyawan obj = repo.save(karyawan);

            karyawanDetail.setKaryawan(obj);
            repoKaryawanDetail.save(karyawanDetail);


            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Karyawan berhasil ditambahkan");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
    @Override
    public Map update(Karyawan karyawan) {
        Map map = new HashMap();
        try {
            Karyawan kryw = repo.getById(karyawan.getId());

            if(kryw == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Data karyawan tidak ada");
                return map;
            }
            kryw.setNama(karyawan.getNama());
            kryw.setJenisKelamin(karyawan.getJenisKelamin());
            kryw.setTanggalLahir(karyawan.getTanggalLahir());
            kryw.setAlamat(karyawan.getAlamat());
            kryw.setStatus(karyawan.getStatus());
            repo.save(kryw);

            map.put("data", kryw);
            map.put("statusCode", "200");
            map.put("statusMessage", "Karyawan berhasil diupdate");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
    @Override
    public Map delete(Long karyawanId) {
        Map map = new HashMap();
        try {
            Karyawan kryw = repo.getById(karyawanId);
            if(kryw == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Data karyawan tidak ada");
                return map;
            }
            repo.deleteById(kryw.getId());

            map.put("statusCode", "200");
            map.put("statusMessage", "Karyawan berhasil dihapus");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
    @Override
    public long count(){
        return repo.count();
    }
}
