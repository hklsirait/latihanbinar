package com.example.latihan.Latihan7.Controller;

import com.example.latihan.Latihan7.Entity.Rekening;
import com.example.latihan.Latihan7.Service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/binar/rekening")
public class RekeningController {

    @Autowired
    RekeningService servis;


    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll(){
        Map rekenings = servis.getAll();
        return new ResponseEntity<Map>(rekenings, HttpStatus.OK);
    }

    @PostMapping("{karyawanId}")
    public ResponseEntity<Map> save(@PathVariable() Long karyawanId, @Valid @RequestBody Rekening rekening){
        Map map = new HashMap();
        Map rkg = servis.insert(rekening, karyawanId);

        map.put("Request= ", rekening);
        map.put("Response ", rkg);
        return new ResponseEntity<Map>(rkg, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Rekening rekening){

        Map map = new HashMap();
        Map rkg = servis.update(rekening);

        map.put("Request= ", rekening);
        map.put("Response ", rkg);
        return new ResponseEntity<Map>(rkg, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id){
        Map rkg = servis.delete(id);
        return new ResponseEntity<Map>(rkg, HttpStatus.OK);
    }
}
