package com.example.latihan.Latihan7.Controller;

import com.example.latihan.Latihan7.Entity.TrainingKaryawan;
import com.example.latihan.Latihan7.Service.TrainingKaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/binar/trainingkaryawan")
public class TrainingKaryawanController {

    @Autowired
    public TrainingKaryawanService service;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody TrainingKaryawan objModel){
        Map map = new HashMap();
        Map obj = service.insert(objModel);

        map.put("Request =", objModel);
        map.put("Response =", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

}