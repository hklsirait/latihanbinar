package com.example.latihan.Latihan7.Entity;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "karyawan_detail")
public class KaryawanDetail implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nik", nullable = false, length = 20)
    private String nik;

    @Column(name = "npwp", nullable = false, length = 20)
    private String npwp;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "karyawan_id", referencedColumnName = "id")
    private Karyawan karyawan;

    public KaryawanDetail(){

    }

    public KaryawanDetail(String nik, String npwp, Karyawan karyawan){
        this.nik = nik;
        this.npwp = npwp;
        this.karyawan = karyawan;
    }

}
