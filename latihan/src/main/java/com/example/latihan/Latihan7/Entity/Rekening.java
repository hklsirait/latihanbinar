package com.example.latihan.Latihan7.Entity;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "rekening")
@Where(clause = "deleted_date is null")
public class Rekening extends AbstractDate implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nama", nullable = false, length = 45)
    private String nama;

    @Column(name = "jenis", nullable = false, length = 45)
    private String jenis;

    @Column(name = "nomor", nullable = false, length = 30)
    private String nomor;

    @JsonIgnore
    @ManyToOne(targetEntity = Karyawan.class, cascade = CascadeType.ALL)
    private Karyawan karyawan;

    public Rekening(){

    }
    public Rekening(String nama, String jenis, String nomor, Karyawan karyawan){
        this.nama = nama;
        this.jenis = jenis;
        this.nomor = nomor;
        this.karyawan = karyawan;
    }

}
