package com.example.latihan.Latihan7.Entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "training")
public class Training implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "judul", nullable = false, columnDefinition = "TEXT")
    private String judul;

    @Column(name = "pengajar", nullable = false, length = 45)
    private String pengajar;

    @OneToMany(mappedBy = "training")
    private List<TrainingKaryawan> trainingKaryawan;

    public Training(){

    }
    public Training(String judul, String pengajar){
        this.judul = judul;
        this.pengajar = pengajar;
    }
}
