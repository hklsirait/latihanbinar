package com.example.latihan.Latihan7.Entity;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import lombok.Data;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "training_karyawan")
public class TrainingKaryawan extends AbstractDate implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "waktu", nullable = false)
    private LocalDate waktu;

    @ManyToOne
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawan;

    @ManyToOne
    @JoinColumn(name = "training_id")
    private Training training;

    public TrainingKaryawan(){

    }
    public TrainingKaryawan(LocalDate waktu, Karyawan karyawan, Training training){
        this.waktu = waktu;
        this.karyawan = karyawan;
        this.training = training;
    }
}
