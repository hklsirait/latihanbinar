package com.example.latihan.Latihan7.Repository;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.example.latihan.Latihan7.Entity.KaryawanDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KaryawanDetailRepository extends JpaRepository<KaryawanDetail, Long> {

}
