package com.example.latihan.Latihan7.Repository;

import com.example.latihan.Latihan7.Entity.Rekening;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RekeningRepository extends JpaRepository<Rekening, Long> {

    @Query("select r from Rekening r")
    public List<Rekening> getAllRekening();

    @Query("select r from Rekening r WHERE r.id = :id")
    public Rekening getById(@Param("id") Long id);


}
