package com.example.latihan.Latihan7.Repository;

import com.example.latihan.Latihan7.Entity.TrainingKaryawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingKaryawanRepository extends JpaRepository<TrainingKaryawan, Long> {

}
