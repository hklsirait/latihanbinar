package com.example.latihan.Latihan7.Repository;

import com.example.latihan.Latihan7.Entity.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepository extends JpaRepository<Training, Long> {

    @Query("select t from Training t WHERE t.id = :id")
    public Training getById(@Param("id") Long id);
}
