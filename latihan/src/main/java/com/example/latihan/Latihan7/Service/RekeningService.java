package com.example.latihan.Latihan7.Service;

import com.example.latihan.Latihan7.Entity.Rekening;

import java.util.Map;

public interface RekeningService {

    public Map getAll();

    public Map insert(Rekening rekening, long karyawanId);

    public Map update(Rekening rekening);

    public Map delete(Long rekeningId);

}
