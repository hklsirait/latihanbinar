package com.example.latihan.Latihan7.Service;

import com.example.latihan.Latihan7.Entity.TrainingKaryawan;

import java.util.Map;

public interface TrainingKaryawanService {
    public Map insert(TrainingKaryawan trainingKaryawan);

    public Map update(TrainingKaryawan trainingKaryawan);
}
