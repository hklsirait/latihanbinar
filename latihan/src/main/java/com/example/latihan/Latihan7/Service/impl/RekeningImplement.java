package com.example.latihan.Latihan7.Service.impl;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.example.latihan.Latihan6.repository.KaryawanRepo;
import com.example.latihan.Latihan7.Entity.Rekening;
import com.example.latihan.Latihan7.Repository.RekeningRepository;
import com.example.latihan.Latihan7.Service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RekeningImplement implements RekeningService {

    @Autowired
    public RekeningRepository rekeningRepository;

    @Autowired
    public KaryawanRepo karyawanRepo;

    @Override
    public Map getAll() {
        List<Rekening> rekenings = new ArrayList<Rekening>();
        Map map = new HashMap();
        try {
            rekenings = rekeningRepository.getAllRekening();
            map.put("data", rekenings);
            map.put("status", "200");
            map.put("message", "Berhasil mengambil semua rekening");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error mengambil data rekening");
            return map;
        }
    }

    @Override
    public Map insert(Rekening rekening, long karyawanId) {
        Map map = new HashMap();
        try {
            Karyawan karyawan = karyawanRepo.getById(karyawanId);
            rekening.setKaryawan(karyawan);
            Rekening obj = rekeningRepository.save(rekening);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }
    @Override
    public Map update(Rekening rekening) {
        Map map = new HashMap();
        try{
            Rekening obj = rekeningRepository.getById(rekening.getId());
            if(obj == null) {
                map.put("status", "404");
                map.put("message", "Data rekening tidak ada");
                return map;
            }
            obj.setNama(rekening.getNama());
            obj.setJenis(rekening.getJenis());
            obj.setNomor(rekening.getNomor());
            rekeningRepository.save(obj);
            map.put("data", obj);
            map.put("status", "200");
            map.put("message", "Success");
            return map;
        }catch (Exception e) {
            e.printStackTrace();
            map.put("status", "500");
            map.put("error", e);
            map.put("message", "Error");
            return map;
        }
    }
    @Override
    public Map delete(Long rekeningId){
        Map map = new HashMap();
        try {
            Rekening rkg = rekeningRepository.getById(rekeningId);
            if(rkg == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Data rekening tidak ada");
                return map;
            }
            rekeningRepository.deleteById(rkg.getId());

            map.put("statusCode", "200");
            map.put("statusMessage", "Data rekening berhasil dihapus");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}
