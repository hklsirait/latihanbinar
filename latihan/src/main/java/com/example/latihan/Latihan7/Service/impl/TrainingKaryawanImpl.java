package com.example.latihan.Latihan7.Service.impl;

import com.example.latihan.Latihan6.entitiy.Karyawan;
import com.example.latihan.Latihan6.repository.KaryawanRepo;
import com.example.latihan.Latihan7.Entity.Rekening;
import com.example.latihan.Latihan7.Entity.Training;
import com.example.latihan.Latihan7.Entity.TrainingKaryawan;
import com.example.latihan.Latihan7.Repository.TrainingKaryawanRepository;
import com.example.latihan.Latihan7.Repository.TrainingRepository;
import com.example.latihan.Latihan7.Service.TrainingKaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class TrainingKaryawanImpl implements TrainingKaryawanService {

    @Autowired
    public TrainingKaryawanRepository trainingKaryawanRepository;

    @Autowired
    public KaryawanRepo repoKaryawan;

    @Autowired
    public TrainingRepository repoTraining;

    @Override
    public Map insert(TrainingKaryawan trainingKaryawan){
        Map map = new HashMap();
        try{
            Karyawan karyawan = repoKaryawan.getById(trainingKaryawan.getKaryawan().getId());
            Training training = repoTraining.getById(trainingKaryawan.getTraining().getId());
            trainingKaryawan.setKaryawan(karyawan);
            trainingKaryawan.setTraining(training);
            TrainingKaryawan obj = trainingKaryawanRepository.save(trainingKaryawan);
            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Karyawan berhasil ditambahkan");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
    @Override
    public Map update(TrainingKaryawan trainingKaryawan){
        return null;
    }
}
